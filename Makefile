TAG=dev-local

venv:
	python -m venv venv

env:
	cp .env.example .env

up_db:
	docker-compose up -d db

migrate:
	./manage.py migrate --no-input

collectstatic:
	./manage.py collectstatic --no-input

docker_build:
	TAG=${TAG} docker-compose build

docker_up:
	TAG=${TAG} docker-compose up -d

docker_collectstatic:
	TAG=${TAG} docker exec -it app ./manage.py collectstatic --no-input