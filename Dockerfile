FROM python:3.8

RUN apt update -y

WORKDIR /app
COPY requirements requirements

RUN pip install -r requirements/local.txt

COPY . .
CMD ["uwsgi", "--http", ":8000", "--module", "main.wsgi"]