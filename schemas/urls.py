from django.urls import path

from schemas.views import CreateSchemaView, ListSchemaView, EditSchemaView

app_name = 'schemas'
urlpatterns = [
    path('new', CreateSchemaView.as_view(), name='new'),
    path('list', ListSchemaView.as_view(), name='list'),
    path('edit/<int:pk>', EditSchemaView.as_view(), name='edit'),
]
