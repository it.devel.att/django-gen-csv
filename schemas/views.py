from django.db import transaction
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView

from schemas.forms import ColumnFormSet, SchemaForm
from schemas.models import Schema


class CreateSchemaView(CreateView):
    form_class = SchemaForm
    model = Schema
    success_url = reverse_lazy('schemas:list')
    template_name = 'create.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data['columns'] = ColumnFormSet(self.request.POST)
        else:
            data['columns'] = ColumnFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        columns = context['columns']
        with transaction.atomic():
            # form.instance.user = self.request.user
            self.object = form.save()
            if columns.is_valid():
                columns.instance = self.object
                columns.save()
        return super().form_valid(form)

    def get_success_url(self):
        return self.success_url


class EditSchemaView(UpdateView):
    model = Schema
    form_class = SchemaForm
    template_name = 'edit.html'
    success_url = reverse_lazy('schemas:list')

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data['columns'] = ColumnFormSet(self.request.POST, instance=self.object)
        else:
            data['columns'] = ColumnFormSet(instance=self.object)
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        columns = context['columns']
        with transaction.atomic():
            # form.instance.user = self.request.user
            self.object = form.save()
            if columns.is_valid():
                columns.instance = self.object
                columns.save()
        return super().form_valid(form)

    def get_success_url(self):
        return self.success_url


class ListSchemaView(ListView):
    queryset = Schema.objects.all().order_by('-updated_at')
    template_name = 'list.html'
