import factory
from factory.fuzzy import FuzzyChoice

from schemas.factories.user import UserFactory
from schemas.models import Schema, COLUMN_SEPARATOR_CHOICES, STRING_CHARACTER_CHOICES


class SchemaFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Schema

    user = factory.SubFactory(UserFactory)
    title = factory.Sequence(lambda i: f'Title {i}')
    column_separator = factory.fuzzy.FuzzyChoice([choice[0] for choice in COLUMN_SEPARATOR_CHOICES])
    string_character = FuzzyChoice([choice[0] for choice in STRING_CHARACTER_CHOICES])
