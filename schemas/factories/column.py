import factory
from factory.fuzzy import FuzzyChoice, FuzzyInteger

from schemas.factories.schema import SchemaFactory
from schemas.models import STRING_CHARACTER_CHOICES, Column, COLUMN_TYPE_CHOICES


class ColumnFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Column

    schema = factory.SubFactory(SchemaFactory)
    name = factory.Sequence(lambda i: f'Column {i}')
    type = FuzzyChoice([choice[0] for choice in COLUMN_TYPE_CHOICES])
    string_character = FuzzyChoice([choice[0] for choice in STRING_CHARACTER_CHOICES])
    from_value = FuzzyInteger(0, 10)
    to_value = FuzzyInteger(0, 10)
    order = FuzzyInteger(0, 10)
