from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.db import models

from common.models import TimestampAbstractModel

COMMA_CHOICE = ','
COLUMN_SEPARATOR_CHOICES = (
    (COMMA_CHOICE, _('Comma')),
)

DOUBLE_QUOTE_CHOICE = "\""
STRING_CHARACTER_CHOICES = (
    (DOUBLE_QUOTE_CHOICE, _('Double-quote')),
)


class Schema(TimestampAbstractModel):
    user = models.ForeignKey(
        get_user_model(),
        related_name='schemas',
        on_delete=models.CASCADE,
        # TODO Remove it. Use user from request
        null=True,
        blank=True,
    )
    title = models.CharField(
        verbose_name=_('Title'),
        max_length=255,
    )
    column_separator = models.CharField(
        verbose_name=_('Column separator'),
        max_length=255,
        default=COMMA_CHOICE,
        choices=COLUMN_SEPARATOR_CHOICES,
    )
    string_character = models.CharField(
        verbose_name=_('String character'),
        max_length=255,
        default=DOUBLE_QUOTE_CHOICE,
        choices=STRING_CHARACTER_CHOICES,
    )

    class Meta:
        verbose_name = _('Schema')
        verbose_name_plural = _('Schemas')


FULL_NAME_TYPE = 'full name'
JOB_TYPE = 'job'
EMAIL_TYPE = 'email'
DOMAIN_TYPE = 'domain name'
PHONE_NUMBER_TYPE = 'phone number'
INTEGER_TYPE = 'integer'
# Company name
# Text (with specified range for a number of sentences)
# Integer (with specified range)
# Address
# Date
COLUMN_TYPE_CHOICES = (
    (FULL_NAME_TYPE, _('Full name')),
    (JOB_TYPE, _('Job')),
    (EMAIL_TYPE, _('Email')),
    (DOMAIN_TYPE, _('Domain')),
    (PHONE_NUMBER_TYPE, _('Phone number')),
    (INTEGER_TYPE, _('Integer')),
)


class Column(models.Model):
    schema = models.ForeignKey(
        Schema,
        related_name='columns',
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        verbose_name=_('Column name'),
        max_length=255,
    )
    type = models.CharField(
        verbose_name=_('Column type'),
        max_length=255,
        choices=COLUMN_TYPE_CHOICES,
        default=FULL_NAME_TYPE,
    )
    from_value = models.IntegerField(
        null=True,
        blank=True,
    )
    to_value = models.IntegerField(
        null=True,
        blank=True,
    )
    order = models.IntegerField(default=0)

    class Meta:
        verbose_name = _('Column')
        verbose_name_plural = _('Columns')
