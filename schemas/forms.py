from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, Fieldset, HTML, ButtonHolder, Submit
from django import forms
from django.forms import inlineformset_factory

from schemas.custom_layout_object import Formset
from schemas.models import Schema, Column


class ColumnForm(forms.ModelForm):
    class Meta:
        model = Column
        exclude = ()


class SchemaForm(forms.ModelForm):
    class Meta:
        model = Schema
        exclude = (
            'user',
            'created_at',
            'updated_at,',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3 create-label'
        self.helper.field_class = 'col-md-9'
        self.helper.layout = Layout(
            Div(
                Field('title'),
                Field('column_separator'),
                Field('string_character'),
                Fieldset(
                    'Schemas columns',
                    Formset('columns')
                ),
                HTML("<br>"),
                ButtonHolder(Submit('submit', 'Save')),
            )
        )


ColumnFormSet = inlineformset_factory(
    Schema,
    Column,
    form=ColumnForm,
    fields=('name', 'type', 'from_value', 'to_value', 'order'),
    extra=1,
    can_delete=True,
)
